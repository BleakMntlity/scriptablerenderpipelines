﻿Shader "SRP/AlphaBlended"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	
	SubShader
	{
		Tags { "RenderType"="AlphaBlended" "Queue"="Transparent"}
		LOD 100

		UsePass "Hidden/TransparentShadowReceiver/SHADOWRECEIVER"
	    UsePass "Hidden/TransparentShadowCaster/SHADOWCASTER"
	}
}
