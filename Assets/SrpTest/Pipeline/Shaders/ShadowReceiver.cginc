struct appdata
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
};

struct v2f
{
    float2 uv : TEXCOORD0;
    RECEIVE_SHADOWS(1);
    float3 worldSpaceNormal: NORMAL;
    float4 vertex : SV_POSITION;
};

sampler2D _MainTex;
float4 _MainTex_ST;
DECLARE_SHADOW_MAP;

v2f vert (appdata v)
{
    v2f o;
    
    o.worldSpaceNormal = mul(v.normal, (float3x3)unity_WorldToObject);
    o.vertex = UnityObjectToClipPos(v.vertex);
    o.uv = TRANSFORM_TEX(v.uv, _MainTex);
    TRANSFER_SHADOW(v.vertex);
    return o;
}

fixed4 frag (v2f i) : SV_Target
{
    fixed4 col = tex2D(_MainTex, i.uv);
    col.rgb *= min(max(dot(i.worldSpaceNormal, _WorldSpaceLightPos0.xyz), 0), 1);
    col = SHADOW_COLOR(col);
    
    return col;
}