﻿Shader "Hidden/OpaqueShadowCaster"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass 
		{
		    Name "ShadowCaster"
		    Tags {"LightMode" = "ShadowCaster"}

		    CGPROGRAM
		    #pragma vertex vert
		    #pragma fragment frag
		    
			#include "UnityCG.cginc"
			
		    struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _ShadowBias;
			float3 _LightDirection;
			
			v2f vert (appdata v)
			{
				v2f o;
    			o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z -= _ShadowBias.x;

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
    			return fixed4(0,0,0,1);
			}
		    ENDCG
		}
	}
}
