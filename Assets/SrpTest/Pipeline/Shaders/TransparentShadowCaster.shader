﻿Shader "Hidden/TransparentShadowCaster"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "RenderQueue"="Transparent" }
		LOD 100

		Pass 
		{
		    BlendOp Add
		    Blend OneMinusDstAlpha DstAlpha
		    Name "ShadowCaster"
		    Tags {"LightMode" = "ShadowCaster"}

		    CGPROGRAM
		    #pragma vertex vert
		    #pragma fragment frag
		    
			#include "UnityCG.cginc"
			
		    struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
			    float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _ShadowBias;
			float3 _LightDirection;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
    			o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z -= _ShadowBias.x;

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
			    fixed4 texCol = tex2D(_MainTex, i.uv);
    			return texCol;
			}
		    ENDCG
		}
	}
}
