﻿Shader "Hidden/TransparentShadowReceiver"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
	    Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{

            Name "ShadowReceiver"
			Tags { "LightMode" = "BasicPass" }
			ZWrite off
		    Blend SrcAlpha OneMinusSrcAlpha
			

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Shadows.cginc"
			#include "ShadowReceiver.cginc"
			ENDCG
		}
	}
}
