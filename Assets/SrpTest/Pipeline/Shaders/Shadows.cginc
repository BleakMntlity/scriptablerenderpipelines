#define DECLARE_SHADOW_MAP sampler2D _ShadowDepth; \
                           sampler2D _ShadowColor; \
                           float4x4 _ShadowMatrix 
                           
#define RECEIVE_SHADOWS(idx) float3 shadowCoords : TEXCOORD##idx

#define TRANSFER_SHADOW(pos) float4 worldPos = mul(unity_ObjectToWorld, pos); \
                             float4 shadowCoords = mul(_ShadowMatrix, worldPos);\
                             o.shadowCoords.xyz = ((shadowCoords.xyz / shadowCoords.w) + 1) * 0.5

#define SHADOW_COLOR(col) shadowColor(_ShadowColor, _ShadowDepth, i.shadowCoords, col)

fixed4 shadowColor(sampler2D shadowColorMap, sampler2D _shadowDepthMap, float3 shadowCoords, fixed4 color)
{
    fixed lit = shadowCoords.z < tex2D(_shadowDepthMap, shadowCoords).r; 
    if (lit < 0.1)
    {
        fixed4 shadowColor = tex2D(shadowColorMap, shadowCoords); 
        color.rgb *= shadowColor.a * shadowColor.rgb;
    }
    return color;                  
}
