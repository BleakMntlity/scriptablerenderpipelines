﻿Shader "SRP/BasicPass"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		UsePass "Hidden/OpaqueShadowReceiver/SHADOWRECEIVER"
		UsePass "Hidden/OpaqueShadowCaster/SHADOWCASTER"
	}
}
