﻿Shader "Hidden/BasicPassNoShadow"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		UsePass "Hidden/OpaqueShadowReceiver/SHADOWRECEIVER"
	}
}
