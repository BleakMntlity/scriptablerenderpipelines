using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace SrpTest.Pipeline
{
	[ExecuteInEditMode]
	public class RenderPipelineAsset : UnityEngine.Experimental.Rendering.RenderPipelineAsset
	{
		public RenderTexture ShadowDepth;
		public RenderTexture ShadowColor;
		
		protected override IRenderPipeline InternalCreatePipeline()
		{
			return new RenderPipeline(ShadowDepth, ShadowColor);
		}
	}
}