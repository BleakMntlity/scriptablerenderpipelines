using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using Object = System.Object;

namespace SrpTest.Pipeline
{
	public class RenderPipeline : UnityEngine.Experimental.Rendering.RenderPipeline
	{
		private readonly RenderTexture _shadowColor;
		private readonly RenderTexture _shadowDepth;
		private readonly CommandBuffer _shadowPass;
		private readonly CommandBuffer _drawPass;

		private readonly RenderTargetIdentifier[] _renderTargetIds;
		
		public RenderPipeline(RenderTexture shadowDepth, RenderTexture shadowColor)
		{
			_shadowDepth = shadowDepth;
			_shadowColor = shadowColor;

			_renderTargetIds = new[] {new RenderTargetIdentifier(_shadowColor), new RenderTargetIdentifier(_shadowDepth)};
			
			_shadowPass = new CommandBuffer();
			_drawPass = new CommandBuffer();
		}

		public override void Render(ScriptableRenderContext context, Camera[] cameras)
		{
			base.Render(context, cameras);

			foreach (var camera in cameras)
			{
				// Culling
				ScriptableCullingParameters cullingParams;
				if (!CullResults.GetCullingParameters(camera, out cullingParams))
					continue;
				
				CullResults cullResults = CullResults.Cull(ref cullingParams, context);

				if (camera.name.Equals("Main Camera", StringComparison.Ordinal))
					RenderShadowMap(context, cullResults);

				_drawPass.ClearRenderTarget(true, true, Color.black);
				// Setup camera for rendering (sets render target, view/projection matrices and other
				// per-camera built-in shader variables).
				context.SetupCameraProperties(camera);
				// clear depth buffer
				context.ExecuteCommandBuffer(_drawPass);
				_drawPass.Clear();

				var settings = new DrawRendererSettings(camera, new ShaderPassName("BasicPass")) {sorting = {flags = SortFlags.CommonOpaque}};
				var filterSettings = new FilterRenderersSettings(true) {renderQueueRange = RenderQueueRange.opaque};
				context.DrawRenderers(cullResults.visibleRenderers, ref settings, filterSettings);

				// Draw skybox
				context.DrawSkybox(camera);

				// Draw transparent objects using BasicPass shader pass
				settings.sorting.flags = SortFlags.CommonTransparent;
				filterSettings.renderQueueRange = RenderQueueRange.transparent;
				context.DrawRenderers(cullResults.visibleRenderers, ref settings, filterSettings);

				context.Submit();
			}
		}

		private void RenderShadowMap(ScriptableRenderContext context, CullResults cullResults)
		{
			int mainLightIndex = GetDirectionalLight(cullResults.visibleLights);
			VisibleLight visibleMainLight = cullResults.visibleLights[mainLightIndex];
			Light mainLight = visibleMainLight.light;

			Bounds bounds;
			if (!cullResults.GetShadowCasterBounds(mainLightIndex, out bounds))
				return;

//			Matrix4x4 view, proj;
//			ShadowSplitData splitData;
			int shadowResolution = Mathf.Min(_shadowDepth.width, _shadowDepth.height);
//			if (!cullResults.ComputeDirectionalShadowMatricesAndCullingPrimitives(mainLightIndex, 0, 1, new Vector3(1, 0, 0),
//				Mathf.Min(_shadowMap.width, _shadowMap.height), mainLight.shadowNearPlane,
//				out view, out proj, out splitData))
//			{
//				return;
//			}

			var lightProjection = Matrix4x4.Ortho(-5, 5, -5, 5, 1, 10);
			var world2Light = mainLight.transform.worldToLocalMatrix;
			world2Light.m20 *= -1;
			world2Light.m21 *= -1;
			world2Light.m22 *= -1;
			world2Light.m23 *= -1;
			
			_shadowPass.SetRenderTarget(_shadowColor, _shadowDepth);
			_shadowPass.ClearRenderTarget(true, true, new Color(1,1,1,0));

			var shadowRenderSettings = new DrawShadowsSettings(cullResults, mainLightIndex);

			const float kernelRadius = 3.65f;
			// Scale bias by cascade's world space depth range.
			// Directional shadow lights have orthogonal projection.
			// proj.m22 = -2 / (far - near) since the projection's depth range is [-1.0, 1.0]
			// In order to be correct we should multiply bias by 0.5 but this introducing aliasing along cascades more visible.
			float bias = mainLight.shadowBias * lightProjection.m22;

			double frustumWidth = 2.0 / lightProjection.m00;
			double frustumHeight = 2.0 / lightProjection.m11;
			float texelSizeX = (float) (frustumWidth / shadowResolution);
			float texelSizeY = (float) (frustumHeight / shadowResolution);
			float texelSize = Mathf.Min(texelSizeX, texelSizeY);
			float normalBias = -mainLight.shadowNormalBias * texelSize * kernelRadius;

			Vector3 lightDirection = -visibleMainLight.localToWorld.GetColumn(2);
			_shadowPass.SetGlobalVector("_ShadowBias", new Vector4(bias, normalBias, 0.0f, 0.0f));
			_shadowPass.SetGlobalVector("_LightDirection", new Vector4(lightDirection.x, lightDirection.y, lightDirection.z, 0.0f));

			_shadowPass.SetViewport(new Rect(0, 0, shadowResolution, shadowResolution));
			_shadowPass.EnableScissorRect(new Rect(4, 4, shadowResolution - 8, shadowResolution - 8));

			_shadowPass.SetViewProjectionMatrices(world2Light, lightProjection);
//			_shadowPass.SetViewProjectionMatrices(view, proj);

			context.ExecuteCommandBuffer(_shadowPass);
			_shadowPass.Clear();

			context.DrawShadows(ref shadowRenderSettings);

			_shadowPass.DisableScissorRect();
			context.ExecuteCommandBuffer(_shadowPass);
			_shadowPass.Clear();

			_drawPass.SetGlobalTexture("_ShadowDepth", _shadowDepth);
			_drawPass.SetGlobalTexture("_ShadowColor", _shadowColor);
			_drawPass.SetGlobalMatrix("_ShadowMatrix", lightProjection * world2Light);
		}

		private int GetDirectionalLight(List<VisibleLight> visibleLights)
		{
			var lightCount = visibleLights.Count;
			for (int i = 0; i < lightCount; ++i)
			{
				if (visibleLights[i].lightType == LightType.Directional)
					return i;
			}

			return -1;
		}
	}
}